<!-- heyty_vampire_portrait.iff -->
<!-- heyty_vampire_portrait -->
<!-- They're back at it again, except they're not making magic this time. Only memories. -->
<P g="426121B5" s="17" p="360" n="Vampire Portrait" />

<!-- heyty_portrait_set.iff -->
<!-- heyty_portrait_set -->
<!-- Everyone got something to remember. Wedding day, birthday or even that time we forcefully acted at school's play. Aww, sweet memories. -->
<P g="49018408" s="17" p="360" n="Portrait Set" />

<!-- heyty_fire_portrait.iff -->
<!-- heyty_fire_portrait -->
<!-- Remember that time when you partied hard all night long and the next day you felt like dead? Well, this party was just like that, but literally. -->
<P g="41D23111" s="17" p="360" n="Fire Portrait" />
